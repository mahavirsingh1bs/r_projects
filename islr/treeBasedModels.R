library(ISLR)

Hitters <- na.omit(Hitters)
as_tibble(Hitters) %>% 
  mutate(Salary = log(Salary)) %>% 
  filter(Years < 4.5) %>% 
  summarise(mean = mean(Salary))
  
library(MASS)
Heart <- read.csv2("data/Heart.csv", header = TRUE, sep = ",")
head(Heart)

Heart <- na.omit(Heart)

class(Heart$ChestPain)
class(Heart$Thal)
class(Heart$ChestPain)

summary(Heart$ChestPain)
summary(Heart$Thal)
