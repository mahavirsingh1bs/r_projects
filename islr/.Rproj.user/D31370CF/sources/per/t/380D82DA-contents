library(ISLR)
library(MASS)

library(tidyverse)

head(Default)

plot(Default$balance, Default$income)

Default_tbl <- as_tibble(Default)
Default_tbl %>% 
  ggplot(aes(balance, income)) + 
  geom_point(aes(color = default))

Default_tbl %>% 
  ggplot() + 
  geom_boxplot(aes(default, balance))

Default_tbl %>% 
  ggplot() + 
  geom_boxplot(aes(default, income))

?geom_point

# Lab: Logistic Regression, LDA, QDA and KNN ------------------------------


# The stock market data ---------------------------------------------------

library(ISLR)
names(Smarket)
dim(Smarket)
head(Smarket)
summary(Smarket)
?Smarket

cor(Smarket)
cor(Smarket[, -9])

plot(Smarket$Volume, Smarket$Year)


# Logistic Regression -----------------------------------------------------

glm.fits <- glm(Direction ~ Lag1 + Lag2 + Lag3 + Lag4 + Lag5 + Volume, data = Smarket, family = binomial)
summary(glm.fits)

coef(glm.fits)
summary(glm.fits)$coef
summary(glm.fits)$coef[, 4]

glm.probs <- predict(glm.fits, type = "response")
glm.probs[1:10]

contrasts(Smarket$Direction)

glm.pred <- rep("Down", 1250)
glm.pred[glm.probs > .5] <- "Up"

table(glm.pred, Smarket$Direction)

train <- (Smarket$Year < 2005)
Smarket.2005 <- Smarket[!train, ]
dim(Smarket.2005)

Direction.2005 <- Smarket$Direction[!train]
head(Direction.2005)

glm.fits <- glm(Direction ~ Lag1 + Lag2 + Lag3 + Lag4 + Lag5 + Volume, data = Smarket, family = binomial, subset = train)
glm.probs <- predict(glm.fits, Smarket.2005, type = "response")

glm.pred <- rep("Down", 252)
glm.pred[glm.probs > .5] <- "Up"

table(glm.pred, Direction.2005)
mean(glm.pred == Direction.2005)
mean(glm.pred != Direction.2005)


glm.fits <- glm(Direction ~ Lag1 + Lag2, data = Smarket, family = binomial, subset = train)
glm.probs <- predict(glm.fits, Smarket.2005, type = "response")

glm.pred <- rep("Down", 252)
glm.pred[glm.probs > .5] <- "Up"

table(glm.pred, Direction.2005)
mean(glm.pred == Direction.2005)
mean(glm.pred != Direction.2005)

predict(glm.fits, newdata = data.frame(Lag1=c(1.2 ,1.5) ,
    Lag2=c(1.1 , -0.8) ), type = "response")

# Linear Discriminant Analysis --------------------------------------------

library(MASS)
lda.fit <- lda(Direction ~ Lag1 + Lag2, data = Smarket, subset = train)
lda.fit

plot(lda.fit)

Smarket_train <- Smarket[train, ]
Smarket_test <- Smarket[!train, ]
dim(Smarket_train)
dim(Smarket_test)

sum(Smarket[train, ]$Direction == "Up") / nrow(Smarket[train, ])
sum(Smarket[train, ]$Direction == "Down") / nrow(Smarket[train, ])

Smarket_train_up <- Smarket_train[Smarket_train$Direction == "Up", ]
Smarket_train_down <- Smarket_train[Smarket_train$Direction == "Down", ]

u1_lag1 <- mean(Smarket_train_up$Lag1)
u1_lag2 <- mean(Smarket_train_up$Lag2)

u2_lag1 <- mean(Smarket_train_down$Lag1)
u2_lag2 <- mean(Smarket_train_down$Lag2)

sum(Smarket_train_up$Lag1 - u1_lag1) / 1248
sum(Smarket_train_up$Lag2 - u1_lag2) / 1248

var(Smarket_train_up$Lag1) / 1248

sum(Smarket_train_down$Lag1 - u2_lag1) / 1248
sum(Smarket_train_down$Lag2 - u2_lag2) / 1248

mean(Smarket_train_down$Lag1)
mean(Smarket_train_down$Lag2)



mean(Smarket[Smarket[train, ]$Direction == "Up", ]$Lag1)
mean(Smarket[Smarket[train, ]$Direction == "Up", ]$Lag2)
mean(Smarket[Smarket[train, ]$Direction == "Down", ]$Lag1)
mean(Smarket[Smarket[train, ]$Direction == "Down", ]$Lag2)

lda.pred <- predict(lda.fit, Smarket.2005)
names(lda.pred)

lda.pred$class
lda.pred$x

head(Smarket.2005)
lda.fit

coef(lda.fit)[1] * Smarket.2005[1:6, ]$Lag1 + coef(lda.fit)[2] * Smarket.2005[1:6, ]$Lag2
head(lda.pred$x)

coef(lda.fit)[1]

lda.class <- lda.pred$class
table(lda.class, Direction.2005)
mean(lda.class == Direction.2005)

sum(lda.pred$posterior[, 1] >= .5)
sum(lda.pred$posterior[, 1] < .5)

lda.pred$posterior [1:20 ,2]
lda.class [1:20]
lda.pred$posterior


# Quadratic Discriminant Analysis -----------------------------------------

qda.fit <- qda(Direction ~ Lag1 + Lag2, data = Smarket, subset = train)
qda.fit

qda.class <- predict(qda.fit, Smarket.2005)$class
table(qda.class ,Direction.2005)

# K-Nearest Neighbors -----------------------------------------------------

library(class)
train.X <- cbind(Smarket$Lag1, Smarket$Lag2)[train, ]
test.X <- cbind(Smarket$Lag1, Smarket$Lag2)[!train, ]
train.Direction <- Smarket$Direction[train]
head(train.X)

set.seed(1)
knn.pred <- knn(train.X, test.X, train.Direction, k = 1)
table(knn.pred, Direction.2005)
(83 + 43) / 252

knn.pred <- knn(train.X, test.X, train.Direction, k = 3)
table(knn.pred, Direction.2005)
mean(knn.pred == Direction.2005)

# An application to Caravan Insurance Data --------------------------------

library(ISLR)
summary(Caravan$Purchase)

standardized.X <- scale(Caravan[, -86])
var(Caravan[, 1])
var(Caravan[, 2])

var(standardized.X[, 1])
var(standardized.X[, 2])

test <- 1:1000
train.X <- standardized.X[-test,]
test.X <- standardized.X[test,]

train.Y <- Caravan$Purchase[-test]
test.Y <- Caravan$Purchase[test]

set.seed(1)
knn.pred <- knn(train.X, test.X, train.Y, k = 1)
mean(test.Y != knn.pred)
mean(test.Y != "No")

table(knn.pred, test.Y)

knn.pred <- knn(train.X, test.X, train.Y, k = 3)
table(knn.pred, test.Y)

e <- exp(-6 + 0.05 * 40 + 1 * 3.5)
px <- e / (1 + e)
px

e <- exp(-6 + 0.05 * 50 + 1 * 3.5)
px <- e / (1 + e)
px

pi

cat1 <- 0.80 * exp( - (1 / 72) * 36)
cat2 <- 0.20 * exp( - (1 / 72) * 16)


cat1 / (cat1 + cat2)

.37 / .63

16 / 1.16

.37 / 1.37

.16/.84


# Exercises: Applied ------------------------------------------------------

# 10
library(ISLR)

names(Weekly)
dim(Weekly)
summary(Weekly)
plot(Weekly$Volume, Weekly$Year)
plot(Weekly$Direction, Weekly$Lag1)
plot(Weekly$Direction, Weekly$Lag2)
plot(Weekly$Direction, Weekly$Lag3)
plot(Weekly$Direction, Weekly$Lag4)
plot(Weekly$Direction, Weekly$Lag5)
plot(Weekly$Direction, Weekly$Volume)

glm.fits <- glm(Direction ~ Lag1 + Lag2 + Lag3 + Lag4 + Lag5 + Volume, data = Weekly, family = binomial)
summary(glm.fits)

coef(glm.fits)

glm.probs <- predict(glm.fits, type = "response")
glm.probs[1:10]

dim(Weekly)
glm.pred <- rep("Down", 1089)
glm.pred[glm.probs > .5] <- "Up"
table(glm.pred, Weekly$Direction)

(54 + 557) / 1089

48 / (48 + 557)

430 / (54 + 430)

54 + 430
sum(Weekly$Direction == "Down")
sum(Weekly$Direction == "Up")

train <- Weekly$Year < 2009
Weekly_train <- Weekly[train, ]
Weekly_test <- Weekly[!train, ]

glm.fits <- glm(Direction ~ Lag2, data = Weekly_train, family = binomial)
summary(glm.fits)

glm.probs <- predict(glm.fits, newdata = Weekly_test, type = "response")

dim(Weekly_test)
glm.probs[1:10]

glm.pred <- rep("Down", 104)
glm.pred[glm.probs > .5] <- "Up"
glm.pred

table(glm.pred, Weekly_test$Direction)
65/104
34 / 43
5 / 61

lda.fit <- lda(Direction ~ Lag2, data = Weekly_train)
lda.fit

plot(lda.fit)

lda.pred <- predict(lda.fit, newdata = Weekly_test, type = "response")
names(lda.pred)
lda.class <- lda.pred$class
head(lda.class)

table(lda.class, Weekly_test$Direction)

mean(lda.class == Weekly_test$Direction)

65/104
34 / 43
5 / 61


qda.fit <- qda(Direction ~ Lag2, data = Weekly_train)
qda.fit

plot(qda.fit)

qda.pred <- predict(qda.fit, newdata = Weekly_test, type = "response")
names(qda.pred)
qda.class <- qda.pred$class
head(qda.class)

table(qda.class, Weekly_test$Direction)

mean(qda.class == Weekly_test$Direction)

65/104
34 / 43
5 / 61

64 /104

library(class)
train.X <- data.frame(cbind(Weekly$Lag2)[train,])
test.X <- data.frame(cbind(Weekly$Lag2)[!train,])
train.Direction <- Weekly$Direction[train]

dim(train.X)
dim(test.X)

set.seed(1)
knn.pred <- knn(train.X, test.X, train.Direction, k = 1)
table(knn.pred, Weekly_test$Direction)

52/ 104
30 / 61
22 / 43

# i
glm.fits <- glm(Direction ~ Lag1 + Lag2 + Lag1:Lag2 + Volume, data = Weekly_train, family = binomial)
summary(glm.fits)

glm.probs <- predict(glm.fits, newdata = Weekly_test, type = "response")

dim(Weekly_test)
glm.probs[1:10]

glm.pred <- rep("Down", 104)
glm.pred[glm.probs > .5] <- "Up"
glm.pred

table(glm.pred, Weekly_test$Direction)
56/104
16 / 43
32 / 61

lda.fit <- lda(Direction ~ Lag1 + Lag2 + Lag1:Lag2, data = Weekly_train)
lda.fit

plot(lda.fit)

lda.pred <- predict(lda.fit, newdata = Weekly_test, type = "response")
names(lda.pred)
lda.class <- lda.pred$class
head(lda.class)

table(lda.class, Weekly_test$Direction)

mean(lda.class == Weekly_test$Direction)

65/104
34 / 43
5 / 61


qda.fit <- qda(Direction ~ Lag1 + Lag2 + Lag1:Lag2, data = Weekly_train)
qda.fit

plot(qda.fit)

qda.pred <- predict(qda.fit, newdata = Weekly_test, type = "response")
names(qda.pred)
qda.class <- qda.pred$class
head(qda.class)

table(qda.class, Weekly_test$Direction)

mean(qda.class == Weekly_test$Direction)

65/104
34 / 43
5 / 61

64 /104

library(class)
train.X <- cbind(Weekly$Lag1, Weekly$Lag2, Weekly$Lag1*Weekly$Lag2)[train,]
test.X <- cbind(Weekly$Lag1, Weekly$Lag2, Weekly$Lag1*Weekly$Lag2)[!train,]
train.Direction <- Weekly$Direction[train]

dim(train.X)
dim(test.X)

set.seed(1)
knn.pred <- knn(train.X, test.X, train.Direction, k = 5)
table(knn.pred, Weekly_test$Direction)

52/ 104
30 / 61
22 / 43

# 11
medMpg <- median(Auto$mpg)
mpg01 <- rep(0, nrow(Auto))
mpg01[Auto$mpg > medMpg] <- 1
Auto01 <- data.frame(Auto, mpg01 = mpg01)
head(Auto01, 20)

summary(Auto01[,-c(1, 9)])
pairs(Auto01[, -c(1, 9)])

names(Auto01)
plot(as.factor(Auto01$mpg01), Auto01$cylinders)
plot(as.factor(Auto01$mpg01), Auto01$displacement)
plot(as.factor(Auto01$mpg01), Auto01$origin)

dim(Auto01)

(20 / 392) * 392

3.92 * 20

392 - 78

train <- 1:314
Auto01_train <- Auto01[train, ]
Auto01_test <- Auto01[-train, ]

lda.fit <- lda(mpg01 ~ .-name, data = Auto01_train)
lda.fit

lda.probs <- predict(lda.fit, newdata = Auto01_test, type = "response")

table(lda.probs$class, Auto01_test$mpg01)

74 / 78

qda.fit <- qda(mpg01 ~ .-name, data = Auto01_train)
qda.fit

qda.probs <- predict(qda.fit, newdata = Auto01_test, type = "response")

table(qda.probs$class, Auto01_test$mpg01)

# f.
names(Auto01)
Auto01$mpg01 <- as.factor(Auto01$mpg01)
glm.fits <- glm(mpg01 ~ cylinders + displacement + horsepower + weight + acceleration + year + origin, data = Auto01_train, family = binomial)
summary(glm.fits)

glm.probs <- predict(glm.fits, newdata = Auto01_test, type = "response")
glm.pred <- rep(0, 78)
glm.pred[glm.probs * 100 > medMpg] <- 1

glm.probs

medMpg

glm.pred

table(glm.pred, Auto01_test$mpg01)
 
73 / 78

# g
library(class)
train.X <- Auto01_train[, -c(1, 9, 10)]
test.X <- Auto01_test[, -c(1, 9, 10)]
train.mpg01 <- Auto01_train$mpg01

dim(train.X)
dim(test.X)

head(Auto01)

set.seed(1)



knn.pred <- knn(train.X, test.X, train.mpg01, k = 1)
table(knn.pred, Auto01_test$mpg01)

knn.pred <- knn(train.X, test.X, train.mpg01, k = 3)
table(knn.pred, Auto01_test$mpg01)

knn.pred <- knn(train.X, test.X, train.mpg01, k = 100)
table(knn.pred, Auto01_test$mpg01)

Power <- function() {
  print(2 ^ 3)
}

Power2 <- function(a, b) {
  print(a ^ b)
}
Power2(3, 8)
Power2(10, 3)
Power2(8, 17)
Power2(131, 3)

Power3 <- function(a, b) {
  return(a ^ b)
}

x <- 1:10
y <- Power3(x, 2)
y

par(mfrow = c(1, 1))
plot(x, y, xlab = "x", ylab = "x^2", main = "Scatterplot of number powers", log="xy")

PlotPower <- function(x, a) {
  y <- Power3(x, a)
  plot(x, y, xlab = "x", ylab = "x^2", main = "Scatterplot of number powers", log="y")
}

PlotPower(1:10000, 10)

# 13
head(Boston)
Boston$crim01 <- rep(0, nrow(Boston))
Boston$crim01[Boston$crim > median(Boston$crim)] <- 1
Boston$crim01

dim(Boston_test)
Boston$crim01

5.06 * 20

train <- 1:405

Boston_train <- Boston[train, ]
Boston_test <- Boston[-train, ]

cor(Boston)

glm.fits <- glm(crim01 ~ .-crim -crim01, data = Boston_train, family = binomial)
summary(glm.fits)

glm.probs <- predict(glm.fits, newdata = Boston_test, type = "response")
length(glm.probs)
med <- median(Boston$crim)
med
glm.pred <- rep(0, 101)
glm.pred[glm.probs > median(Boston$crim)] <- 1

table(glm.pred, Boston_test$crim01)

86 / 101

glm.pred
glm.probs
median(Boston$crim)

lda.fit <- lda(crim01 ~ .-crim -crim01, data = Boston_train)
lda.fit

lda.pred <- predict(lda.fit, newdata = Boston_test, type = "response")
lda.pred$class
table(lda.pred$class, Boston_test$crim01)

head(Boston_train)

train.X <- Boston_train[, -c(1, 15)]
train.Y <- Boston_train[, 15]
test.X <- Boston_test[, -c(1, 15)]
test.Y <- Boston_test[, 15]
dim(train.X)

knn.fit <- knn(train.X, test.X, train.Y, k = 1)
table(knn.fit, test.Y)

93 / 101

knn.fit <- knn(train.X, test.X, train.Y, k = 100)
table(knn.fit, test.Y)

91/101
