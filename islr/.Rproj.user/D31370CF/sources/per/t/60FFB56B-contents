# Lab: Non-linear Modeling ------------------------------------------------

library(ISLR)


# Polynomial Regression and Step Functions --------------------------------

fit <- lm(wage ~ poly(age, 4), data = Wage)
coef(summary(fit))

head(poly(Wage$age, 4, raw = T))

Wage$age[1] ^ 2
Wage$age[1] ^ 3
Wage$age[1] ^ 4

fit2 <- lm(wage ~ poly(age, 4, raw = T), data = Wage)
coef(summary(fit2))

fit2a <- lm(wage ~ age + I(age ^ 2) + I(age ^ 3) + I(age ^ 4), data = Wage)
coef(fit2a)

fit2b <- lm(wage ~ cbind(age, age ^ 2, age ^ 3, age ^ 4), data = Wage)

agelims <- range(Wage$age)
age.grid <- seq(from = agelims[1], to = agelims[2])
preds <- predict(fit, newdata = list(age = age.grid), se = TRUE)
se.bands <- cbind(preds$fit + 2 * preds$se.fit, preds$fit - 2 * preds$se.fit)

par(mfrow = c(1, 2), mar = c(4.5, 4.5, 1, 1), oma = c(0, 0, 4, 0))
plot(Wage$age, Wage$wage, xlim = agelims, cex = .5, col = "darkgrey")
title("Degree-4 Polynomial", outer = T)
lines(age.grid, preds$fit, lwd = 2, col = "blue")
matlines(age.grid, se.bands, lwd = 1, col = "blue", lty = 4)

preds2 <- predict(fit2, newdata = list(age = age.grid), se = TRUE)
max(abs(preds$fit - preds2$fit))


fit.1 <- lm(wage ~ age, data = Wage)
fit.2 <- lm(wage ~ poly(age, 2), data = Wage)
fit.3 <- lm(wage ~ poly(age, 3), data = Wage)
fit.4 <- lm(wage ~ poly(age, 4), data = Wage)
fit.5 <- lm(wage ~ poly(age, 5), data = Wage)

anova(fit.1, fit.2, fit.3, fit.4, fit.5)

coef(summary(fit.5))

fit.1 <- lm(wage ~ education + age, data = Wage)
fit.2 <- lm(wage ~ education + poly(age, 2), data = Wage)
fit.3 <- lm(wage ~ education + poly(age, 3), data = Wage)
anova(fit.1, fit.2, fit.3)

fit <- glm(I(wage > 250) ~ poly(age, 4), data = Wage, family = binomial)
preds <- predict(fit, newdata = list(age = age.grid), se = T)

pfit <- exp(preds$fit) / (1 + exp(preds$fit))
se.bands.logit <- cbind(preds$fit + 2 * preds$se.fit, preds$fit - 2 * preds$se.fit)
se.bands <- exp(se.bands.logit) / (1 + exp(se.bands.logit))

plot(Wage$age, I(Wage$wage > 250), xlim = agelims, type = "n", ylim = c(0, .2))
points(jitter(Wage$age), I((Wage$wage > 250) / 5), cex = .5, pch = "|", col = "darkgrey")
lines(age.grid, pfit, lwd = 2, col = "blue")
matlines(age.grid, se.bands, lwd = 1, col = "blue", lty = 3)

table(cut(Wage$age, 4))
fit <- lm(wage ~ cut(age, 4), data = Wage)
coef(summary(fit))

preds <- predict(fit, newdata = list(age = age.grid), se = TRUE)
se.bands <- cbind(preds$fit + 2 * preds$se.fit, preds$fit - 2 * preds$se.fit)

par(mfrow = c(1, 2), mar = c(4.5, 4.5, 1, 1), oma = c(0, 0, 4, 0))
plot(Wage$age, Wage$wage, xlim = agelims, cex = .5, col = "darkgrey")
title("Degree-4 Polynomial", outer = T)
lines(age.grid, preds$fit, lwd = 2, col = "blue")
matlines(age.grid, se.bands, lwd = 1, col = "blue", lty = 4)

# Splines -----------------------------------------------------------------

library(splines)

# regression splines
fit <- lm(wage ~ bs(age, knots = c(25, 40, 60)), data = Wage)
pred <- predict(fit, newdata = list(age = age.grid), se = T)
plot(Wage$age, Wage$wage, col = "gray")
lines(age.grid, pred$fit, lwd = 2)
lines(age.grid, pred$fit + 2 * pred$se.fit, lty = "dashed")
lines(age.grid, pred$fit - 2 * pred$se.fit, lty = "dashed")

dim(bs(Wage$age, knots = c(25, 40, 60)))
dim(bs(Wage$age, df = 6))
attr(bs(Wage$age, df = 6), "knots")

# natural spline
fit2 <- lm(wage ~ ns(age, df = 4), data = Wage)
pred2 <- predict(fit2, newdata = list(age = age.grid), se = T)
lines(age.grid, pred2$fit, col = "red", lwd = 2)

# smoothing spline
plot(Wage$age, Wage$wage, xlim = agelims, cex = .5, col = "darkgrey")
title("Smoothing spline")
fit <- smooth.spline(Wage$age, Wage$wage, df = 16)
fit2 <- smooth.spline(Wage$age, Wage$wage, cv = TRUE)
fit2$df
lines(fit, col = "red", lwd = 2)
lines(fit2, col = "blue", lwd = 2)
legend("topright", legend =c("16 DF " ,"6.8 DF"), col=c("red "," blue "),lty =1, lwd =2, cex =.8)

plot(Wage$age, Wage$wage, xlim = agelims, cex = .5, col = "darkgrey")
title("Local Regression")
fit <- loess(wage ~ age, span = .2, data = Wage)
fit2 <- loess(wage ~ age, span = .5, data = Wage)
lines(age.grid, predict(fit, data.frame(age = age.grid)), col = "red", lwd = 2)
lines(age.grid, predict(fit2, data.frame(age = age.grid)), col = "blue", lwd = 2)
legend("topright", legend = c("Span=0.2", "Span=0.5"), col = c("red", "blue"), lty = 2, lwd = 2, cex = .8)

# GAMs --------------------------------------------------------------------

gam1 <- lm(wage ~ ns(year, 4) + ns(age, 5) + education, data = Wage)

install.packages("gam")
library(gam)
gam.m3 <- gam(wage ~ s(year, 4) + s(age, 5) + education, data = Wage)

par(mfrow = c(1, 3))
plot(gam.m3, se = TRUE, col = "blue")
plot.Gam(gam1, se = TRUE, col = "red")

gam.m1 <- gam(wage ~ s(age, 5) + education, data = Wage)
gam.m2 <- gam(wage ~ year + s(age, 5) + education, data = Wage)
anova(gam.m1, gam.m2, gam.m3, test = "F")

summary(gam.m3)

preds <- predict(gam.m2, newdata = Wage)

gam.lo <- gam(wage ~ s(year, df = 4) + lo(age, span = 0.7) + education, data = Wage)
plot.Gam(gam.lo, se = TRUE, col = "green")

gam.lo.i <- gam(wage ~ lo(year, age, span = 0.5) + education, data = Wage)
plot(gam.lo.i, se = TRUE, col = "red")

gam.lr <- gam(I(wage > 250) ~ year + s(age, df = 5) + education, family = binomial, data = Wage)
par(mfrow = c(1, 3))
plot(gam.lr, se = TRUE, col = "green")

table(Wage$education, I(Wage$wage > 250))

gam.lr <- gam(I(wage > 250) ~ year + s(age, df = 5) + education, family = binomial, data = Wage, 
              subset = (education != "1. < HS Grad"))
par(mfrow = c(1, 3))
plot(gam.lr, se = TRUE, col = "green")

# Exercises: Applied ------------------------------------------------------

# 6 a.
k <- 10
p <- 10
val.errors <- matrix(NA, k, p)

nrow(Wage)
folds <- sample(1:10, nrow(Wage), replace = TRUE)
head(folds)

for (j in 1:k) {
  for (i in 1:p) {
    lm.fit <- lm(wage ~ poly(age, i), data = Wage[folds != j, ])
    preds <- predict(lm.fit, newdata = Wage[folds == j, ])
    val.errors[j, i] <- mean((preds - Wage[folds == j, ]$wage) ^ 2)
  }
}

which.min(apply(val.errors, 2, mean))

par(mfrow = c(1, 1))
plot(Wage$age, Wage$wage, col = "gray")

lm.fit <- lm(wage ~ poly(age, 9), data = Wage)
lines(Wage$age, predict(lm.fit, newdata = Wage), col = "red")

fit=lm(wage∼cut(age, 2),data=Wage)

# 6 b.
k <- 10
cuts <- 10
val.errors <- matrix(NA, k, cuts)

nrow(Wage)
folds <- sample(1:10, nrow(Wage), replace = TRUE)
head(folds)

for (j in 1:k) {
  for (i in 1:cuts) {
    lm.fit <- lm(wage ~ cut(age, i + 1), data = Wage[folds != j, ])
    matr <- model.matrix(wage ~ cut(age, i + 1), data = Wage[folds != j, ])
    coefi <- coef(lm.fit)
    preds <- matr %*% coefi
    print(head(preds))
    val.errors[j, i] <- mean((preds - Wage[folds == j, ]$wage) ^ 2)
  }
}

?coef
which.min(apply(val.errors, 2, mean))

par(mfrow = c(1, 1))
plot(Wage$age, Wage$wage, col = "gray")

lm.fit <- lm(wage ~ poly(age, 9), data = Wage)
lines(Wage$age, predict(lm.fit, newdata = Wage), col = "red")

# 6
set.seed(1)
library(ISLR)
library(boot)
all.deltas = rep(NA, 10)

for (i in 1:10) {
  glm.fit = glm(wage ~ poly(age, i), data = Wage)
  all.deltas[i] = cv.glm(Wage, glm.fit, K = 10)$delta[2]
}

plot(1:10, all.deltas, xlab = "Degree", ylab = "CV error", type = "l", pch = 20, lwd = 2, ylim = c(1590, 1700))
min.point = which.min(all.deltas)
min.point

sd.points = sd(all.deltas)
lines(1:10, all.deltas + 0.2 * sd.points, col="red", lty="dashed")
lines(1:10, all.deltas - 0.2 * sd.points, col="red", lty="dashed")
legend("topright", "0.2-standard deviation lines", lty="dashed", col="red")

fit.1 = lm(wage~poly(age, 1), data=Wage)
fit.2 = lm(wage~poly(age, 2), data=Wage)
fit.3 = lm(wage~poly(age, 3), data=Wage)
fit.4 = lm(wage~poly(age, 4), data=Wage)
fit.5 = lm(wage~poly(age, 5), data=Wage)
fit.6 = lm(wage~poly(age, 6), data=Wage)
fit.7 = lm(wage~poly(age, 7), data=Wage)
fit.8 = lm(wage~poly(age, 8), data=Wage)
fit.9 = lm(wage~poly(age, 9), data=Wage)
fit.10 = lm(wage~poly(age, 10), data=Wage)
anova(fit.1, fit.2, fit.3, fit.4, fit.5, fit.6, fit.7, fit.8, fit.9, fit.10)

plot(wage~age, data=Wage, col="darkgrey")
agelims = range(Wage$age)
age.grid = seq(from = agelims[1], to = agelims[2])
lm.fit <- lm(wage ~ poly(age, 3), data = Wage)
lm.pred <- predict(lm.fit, data.frame(age = age.grid))
lines(age.grid, lm.pred, col = "blue", lwd = 2)

# cut points
all.cvs = rep(NA, 10)
for (i in 2:10) {
  Wage$age.cut = cut(Wage$age, i)
  lm.fit = glm(wage ~ age.cut, data = Wage)
  all.cvs[i] = cv.glm(Wage, lm.fit, K = 10)$delta[2]
}
plot(2:10, all.cvs[-1], xlab = "Number of cuts", ylab = "CV error", type = "l", pch = 20, lwd = 2)

lm.fit = glm(wage ~ cut(age, 8), data = Wage)
agelims = range(Wage$age)
age.grid = seq(from = agelims[1], to = agelims[2])
lm.pred = predict(lm.fit, list(age = age.grid))
plot(wage ~ age, data = Wage, col = "darkgrey")
lines(age.grid, lm.pred, col = "red", lwd = 2)

# 7
library(ISLR)
head(Wage)

par(mfrow = c(2, 2))
plot(Wage$maritl, Wage$wage)
plot(Wage$jobclass, Wage$wage)
plot(Wage$health, Wage$wage)
plot(Wage$health_ins, Wage$wage)

summary(Wage$maritl)
summary(Wage$jobclass)
summary(Wage$health)
summary(Wage$health_ins)

gam.m4 <- gam(wage ~ s(age, 4) + health + maritl + jobclass, data = Wage)
par(mfrow = c(1, 4))
plot(gam.m4, se = TRUE, col = "red")

# Polymorphism and Step Functions -----------------------------------------

fit <- lm(wage ~ maritl, data = Wage)
deviance(fit)

fit <- lm(wage ~ jobclass, data = Wage)
deviance(fit)

fit <- lm(wage ~ health, data = Wage)
deviance(fit)

fit <- lm(wage ~ health_ins, data = Wage)
deviance(fit)

fit <- lm(wage ~ maritl + jobclass + health + health_ins, data = Wage)
deviance(fit)

# GAM
library(gam)

fit <- gam(wage ~ s(age, 4), data = Wage)
deviance(fit)

summary(fit)

# 8
library(ISLR)
library(splines)
head(Auto)

rangehp = range(Auto$horsepower)
horsepower.grid = seq(from = rangehp[1], to = rangehp[2])

# basic functions
par(mfrow = c(2, 2))
for (i in c(6, 8, 10, 12)) {
  fit <- lm(mpg ~ bs(horsepower, df = i), data = Auto)
  pred <- predict(fit, newdata = list(horsepower = horsepower.grid), se = T)
  plot(Auto$horsepower, Auto$mpg, col = "gray")
  lines(horsepower.grid, pred$fit, lwd = 2, col = "red")
  lines(horsepower.grid, pred$fit + 2 * pred$se.fit, lty = "dashed", lwd = 1, col = "red")
  lines(horsepower.grid, pred$fit - 2 * pred$se.fit, lty = "dashed", lwd = 1, col = "red")
}

# Polynomial regression
par(mfrow = c(2, 2))
for (i in c(3, 6, 9, 12)) {
  fit <- lm(mpg ~ poly(horsepower, i), data = Auto)
  pred <- predict(fit, newdata = list(horsepower = horsepower.grid), se = T)
  plot(Auto$horsepower, Auto$mpg, col = "gray")
  lines(horsepower.grid, pred$fit, lwd = 2, col = "red")
  lines(horsepower.grid, pred$fit + 2 * pred$se.fit, lty = "dashed", lwd = 1, col = "red")
  lines(horsepower.grid, pred$fit - 2 * pred$se.fit, lty = "dashed", lwd = 1, col = "red")
}

# natural spline
par(mfrow = c(2, 2))
for (i in c(4, 6, 8, 10)) {
  fit <- lm(mpg ~ ns(horsepower, df = i), data = Auto)
  pred <- predict(fit, newdata = list(horsepower = horsepower.grid), se = T)
  plot(Auto$horsepower, Auto$mpg, col = "gray")
  lines(horsepower.grid, pred$fit, lwd = 2, col = "red")
  lines(horsepower.grid, pred$fit + 2 * pred$se.fit, lty = "dashed", lwd = 1, col = "red")
  lines(horsepower.grid, pred$fit - 2 * pred$se.fit, lty = "dashed", lwd = 1, col = "red")
}

# Smoothing spline
plot(Auto$horsepower, Auto$mpg, cex = .5, col = "darkgrey")
title("Smoothing spline")
fit <- smooth.spline(Auto$horsepower, Auto$mpg, df = 16)
fit2 <- smooth.spline(Auto$horsepower, Auto$mpg, cv = TRUE)
fit2$df

lines(fit, col = "red", lwd = 2)
lines(fit2, col = "blue", lwd = 2)
legend ("topright",legend=c("16 DF" ,"5.8 DF"),
        col=c("red","blue"),lty=1,lwd=2, cex =.8)

# Local regression
par(mfrow = c(1, 1))
plot(Auto$horsepower, Auto$mpg, cex = .5, col = "darkgrey")
title("Local regression")
fit <- loess(mpg ~ horsepower, span = .2, data = Auto)
fit2 <- loess(mpg ~ horsepower, span = .5, data = Auto)

lines(horsepower.grid, predict(fit, data.frame(horsepower = horsepower.grid)), 
      col = "red", lwd = 2)
lines(horsepower.grid, predict(fit2, data.frame(horsepower = horsepower.grid)), 
      col = "blue", lwd = 2)
legend("topright", legend = c("Span=0.2", "Span=0.5"), col = c("red", "blue"), lty = 1, lwd = 2, cex = .8)

# GAMs
library(gam)
head(Auto)
gam.m3 <- gam(mpg ~ s(horsepower, 4) + s(displacement, 8) + s(weight, 4), data = Auto)
par(mfrow = c(1, 3))
plot(gam.m3, se = TRUE, col = "blue")

gam.m1 <- gam(mpg ~ s(horsepower, 6) + s(displacement, 8) + s(weight, 10), data = Auto)
gam.m2 <- gam(mpg ~ s(horsepower, 10) + s(displacement, 10) + s(weight, 10), data = Auto)
anova(gam.m1, gam.m2, gam.m3)

summary(gam.m3)

# 9
library(ISLR)
library(MASS)

library(glmnet)
library(boot)

disrange <- range(Boston$dis)
dis.grid <- seq(from = disrange[1], to = disrange[2], by = .1)

par(mfrow = c(1, 1))
fit <- glm(nox ~ poly(dis, 3), data = Boston)
plot(Boston$dis, Boston$nox, xlab = "distance to employment centre", 
     ylab = "nitrogen oxide concentration")
summary(fit)
pred <- predict(fit, newdata = list(dis = dis.grid))
lines(dis.grid, pred, col = "red", lwd = 2)

all.rss <- rep(NA, 10)
for (i in 1:10) {
  lm.fit <- lm(nox ~ poly(dis, i), data = Boston)
  all.rss[i] <- sum(lm.fit$residuals ^ 2)
}
all.rss

par(mfrow = c(3, 4))
rss <- rep(NA, 10)
for (i in 1:10) {
  fit <- glm(nox ~ poly(dis, i), data = Boston)
  pred <- predict(fit, newdata = list(dis = dis.grid))
  plot(Boston$dis, Boston$nox)
  lines(dis.grid, pred, col = "red", lwd = 2)
  rss[i] <- cv.glm(Boston, fit, K = 10)$delta[2]
  fit$residuals
}
which.min(rss)
rss

plot(1:10, rss, type = "b")

sum(fit$residuals)

par(mfrow = c(1, 1))
fit <- lm(nox ~ bs(dis, df = 4, knots = c(4, 7, 11)), data = Boston)
pred <- predict(fit, newdata = list(dis = dis.grid))
plot(Boston$dis, Boston$nox)
lines(dis.grid, pred, col = "red", lwd = 2)
summary(fit)

par(mfrow = c(4, 4))
all.cv <- rep(NA, 16)
for (i in 3:16) {
  fit <- lm(nox ~ ns(dis, df = i), data = Boston)
  pred <- predict(fit, newdata = list(dis = dis.grid))
  plot(Boston$dis, Boston$nox)
  lines(dis.grid, pred, col = "red", lwd = 2)
  all.cv[i] <- sum(fit$residuals ^ 2)
}

par(mfrow = c(1, 1))
plot(3:16, all.cv[-c(1, 2)], type = "b")
all.cv

all.cv <- rep(NA, 16)
for (i in 3:16) {
  fit <- glm(nox ~ ns(dis, df = i), data = Boston)
  all.cv[i] <- cv.glm(Boston, fit, K = 10)$delta[2]
}

all.cv[-c(1, 2)]
which.min(all.cv[-c(1, 2)])
par(mfrow = c(1, 1))
plot(3:16, all.cv[-c(1, 2)], type = "b")

# 10
library(leaps)

dim(College)
head(College)
train <- sample(c(TRUE, FALSE), nrow(College), replace = TRUE)
train_set <- College[train, ]
test_set <- College[!train, ]

regfit.full <- regsubsets(Outstate ~ ., data = train_set, method = "forward", nvmax = 17)
reg.summary <- summary(regfit.full)

plot(regfit.full, scale = "Cp")

which.min(reg.summary$cp)
which.min(reg.summary$bic)
which.min(reg.summary$adjr2)

reg.fit <- regsubsets(Outstate ~ ., data = College, method = "forward", nvmax = 17)
coefi <- coef(reg.fit, id = 10)
coefi

# b
gam.fit <- gam(Outstate ~ Private + Apps + Accept + Enroll + Top25perc + 
                 s(Room.Board, 2) + Terminal + perc.alumni + Expend + Grad.Rate, data = train_set)
pred <- predict(gam.fit, newdata = test_set)

par(mfrow = c(3, 4))
plot(gam.fit, se = TRUE, col = "blue")

# 11
x1 <- rnorm(100)
x2 <- rnorm(100)
eps <- rnorm(100)

y <- 1 + 2 * x1 + 3 * x2 + eps

beta1 <- 2
a <- y - beta1 * x1
beta2 <- lm(a ~ x2)$coef[2]
beta2

beta2 <- 3
a = y - beta2 * x2
beta1 <- lm(a ~ x1)$coef[2]
beta1

beta1s <- rep(0, 1000)
beta2s <- rep(0, 1000)

for (i in 1:1000) {
  x1 <- rnorm(100)
  x2 <- rnorm(100)
  eps <- rnorm(100)
  
  y <- 1 + 2 * x1 + 3 * x2 + eps
  
  beta1 <- 2
  a <- y - beta1 * x1
  beta2s[i] <- lm(a ~ x2)$coef[2]
  
  beta2 <- 3
  a = y - beta2 * x2
  beta1s[i] <- lm(a ~ x1)$coef[2]
}

X1 <- rnorm(100)
X2 <- rnorm(100)
eps <- rnorm(100)

Y <- 1 + 2 * x1 + 3 * x2 + eps


beta0 = rep(NA, 1000)
beta1 = rep(NA, 1000)
beta2 = rep(NA, 1000)
beta1[1] = 10

for (i in 1:1000) {
  a = Y - beta1[i] * X1
  beta2[i] = lm(a ~ X2)$coef[2]
  a = Y - beta2[i] * X2
  lm.fit = lm(a ~ X1)
  if (i < 1000) {
    beta1[i + 1] = lm.fit$coef[2]
  }
  beta0[i] = lm.fit$coef[1]
}

plot(1:1000, beta0, type = "l", xlab = "iteration", ylab = "betas", ylim = c(-2.2, 1.6), col = "green")
lines(1:1000, beta1, col = "red")
lines(1:1000, beta2, col = "blue")
legend("center", c("beta0", "beta1", "beta2"), lty = 1, col = c("green", "red", 
                                                                "blue"))
par(mfrow = c(1, 1))
plot(beta1s, col = "red")
plot(beta2s, col = "blue")
range(beta1s)

library(tidyverse)
tibble(beta1s, beta2s) %>% 
  gather(beta1s, beta2s, key = "key", value = "value") %>% 
  ggplot(aes(seq(1, 2, .1), value, color = key)) + 
  geom_point()

lm.fit = lm(Y ~ X1 + X2)
plot(1:1000, beta0, type = "l", xlab = "iteration", ylab = "betas", ylim = c(-2.2, 
                                                                             1.6), col = "green")
lines(1:1000, beta1, col = "red")
lines(1:1000, beta2, col = "blue")
abline(h = lm.fit$coef[1], lty = "dashed", lwd = 3, col = rgb(0, 0, 0, alpha = 0.4))
abline(h = lm.fit$coef[2], lty = "dashed", lwd = 3, col = rgb(0, 0, 0, alpha = 0.4))
abline(h = lm.fit$coef[3], lty = "dashed", lwd = 3, col = rgb(0, 0, 0, alpha = 0.4))
legend("center", c("beta0", "beta1", "beta2", "multiple regression"), lty = c(1, 
                                                                              1, 1, 2), col = c("green", "red", "blue", "black"))

# 12
set.seed(1)
p = 100
n = 1000
x = matrix(ncol = p, nrow = n)
coefi = rep(NA, p)
for (i in 1:p) {
  x[, i] = rnorm(n)
  coefi[i] = rnorm(1) * 100
}
y = x %*% coefi + rnorm(n)

beta = rep(0, p)
max_iterations = 1000
errors = rep(NA, max_iterations + 1)
iter = 2
errors[1] = Inf
errors[2] = sum((y - x %*% beta)^2)
threshold = 1e-04
while (iter < max_iterations && errors[iter - 1] - errors[iter] > threshold) {
  for (i in 1:p) {
    a = y - x %*% beta + beta[i] * x[, i]
    beta[i] = lm(a ~ x[, i])$coef[2]
  }
  iter = iter + 1
  errors[iter] = sum((y - x %*% beta)^2)
  print(c(iter - 2, errors[iter - 1], errors[iter]))
}

plot(1:11, errors[3:13])

# 12
set.seed(1)
p = 100
n = 1000
x = matrix(ncol = p, nrow = n)
coefi = rep(NA, p)
for (i in 1:p) {
  x[, i] = rnorm(n)
  coefi[i] = rnorm(1) * 100
}

y = x %*% coefi + rnorm(n)

beta = rep(0, p)
max_iterations = 1000
errors = rep(NA, max_iterations + 1)
iter = 2
errors[1] = Inf
errors[2] = sum((y - x %*% beta) ^ 2)

threshold = 1e-04
while (iter < max_iterations && errors[iter - 1] - errors[iter] > threshold) {
  for (i in 1:p) {
    a = y - x %*% beta + beta[i] * x[, i]
    beta[i] = lm(a ~ x[, i])$coef[2]
  }
  iter = iter + 1
  errors[iter] = sum((y - x %*% beta) ^ 2)
  print(c(iter - 2, errors[iter - 1], errors[iter]))
}

plot(1:11, errors[3:13])

