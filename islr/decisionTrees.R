
# Fitting Classification trees --------------------------------------------

library(tree)
library(ISLR)

High <- ifelse(Carseats$Sales <= 8, "No", "Yes")
Carseats <- data.frame(Carseats, High)
tree.carseats <- tree(High ~ . -Sales, data = Carseats)
summary(tree.carseats)

plot(tree.carseats)
text(tree.carseats, pretty = 0)

tree.carseats

set.seed(2)
train <- sample(1:nrow(Carseats), 200)
Carseats.test <- Carseats[-train, ]
High.test <- High[-train]
tree.carseats <- tree(High ~ .-Sales, Carseats, subset = train)
tree.pred <- predict(tree.carseats, Carseats.test, type = "class")
table(tree.pred, High.test)

nrow(Carseats.test)
(104 + 50)/200

set.seed(3)
cv.carseats <- cv.tree(tree.carseats, FUN = prune.misclass)
names(cv.carseats)
cv.carseats

par(mfrow = c(1, 2))
plot(cv.carseats$size, cv.carseats$dev, type = "b")
plot(cv.carseats$k, cv.carseats$dev, type = "b")

prune.carseats <- prune.misclass(tree.carseats, best = 21)
plot(prune.carseats)
text(prune.carseats)

tree.pred <- predict(prune.carseats, Carseats.test, type = "class")
table(tree.pred, High.test)


# 5
prune.carseats <- prune.misclass(tree.carseats, best = 5)
plot(prune.carseats)
text(prune.carseats)

tree.pred <- predict(prune.carseats, Carseats.test, type = "class")
table(tree.pred, High.test)
(82 + 67)/200

# Fitting regression trees ------------------------------------------------

library(MASS)
set.seed(1)

train <- sample(1:nrow(Boston), nrow(Boston) / 2)
tree.boston <- tree(medv ~ ., Boston, subset = train)
summary(tree.boston)

par(mfrow = c(1, 1))
plot(tree.boston)
text(tree.boston)

cv.boston <- cv.tree(tree.boston)
plot(cv.boston$size, cv.boston$dev, type = "b")

prune.boston <- prune.tree(tree.boston, best = 5)
plot(prune.boston)
text(prune.boston, pretty = 0)

yhat = predict(tree.boston, newdata = Boston[-train, ])
boston.test = Boston[-train, "medv"]
plot(yhat, boston.test)
abline(0, 1)
mean((yhat - boston.test) ^ 2)

# Bagging and Random Forests ----------------------------------------------

library(randomForest)
set.seed(1)
bag.boston <- randomForest(medv ~ ., data = Boston, subset = train, mtry = 13, importance = TRUE)
bag.boston

yhat.bag <- predict(bag.boston, newdata = Boston[-train, ])
plot(yhat.bag, boston.test)
abline(0, 1)
mean((yhat.bag - boston.test) ^ 2)
sqrt(23.59)

bag.boston <- randomForest(medv ~ ., data = Boston, subset = train, mtry = 13, ntree = 25)
yhat.bag = predict(bag.boston, newdata = Boston[-train, ])
mean((yhat.bag - boston.test) ^ 2)

set.seed(1)
rf.boston = randomForest(medv ~ ., data = Boston, subset = train, mtry = 6, importance = TRUE)
yhat.rf = predict(rf.boston, newdata = Boston[-train, ])
mean((yhat.rf - boston.test) ^ 2)

importance(rf.boston)
varImpPlot(rf.boston)

# Boosting ----------------------------------------------------------------

library(gbm)
set.seed(1)
boost.boston <- gbm(medv ~ ., data = Boston[train, ], distribution = "gaussian", n.trees = 5000, interaction.depth = 4)
summary(boost.boston)

par(mfrow = c(1, 2))
plot(boost.boston, i = "rm")
plot(boost.boston, i = "lstat")

yhat = predict(boost.boston, newdata = Boston[-train, ], n.trees = 5000)
mean((yhat - boston.test) ^ 2)

boost.boston <- gbm(medv ~ ., data = Boston[train, ], distribution = "gaussian", 
                    n.trees = 5000, interaction.depth = 4,
                    shrinkage = 0.2, verbose = F)
yhat = predict(boost.boston, newdata = Boston[-train, ], n.trees = 5000)
mean((yhat - boston.test) ^ 2)

# Exercises: Conceptual ---------------------------------------------------

par(mfrow = c(1, 1), xpd = NA)
plot(NA, NA, type = "n", xlim = c(0, 3000), ylim = c(0, 1500), xlab = "X", ylab = "Y")
lines(x = c(1500, 1500), y = c(0, 1500))
text(x = 1500, y = 1600, labels = c("t1"), col = "red")

p <- seq(0, 1, 0.01)
gini.index <- 2 * p * (1 - p)
class.error <- 1 - pmax(p, 1 - p)
cross.entropy <- - (p * log(p) + (1 - p) * log(1 - p))
cbind(gini.index, class.error, cross.entropy)
matplot(p, cbind(gini.index, class.error, cross.entropy), col = c("red", "green", "blue"))

# Exercises: Applied ------------------------------------------------------

library(randomForest)
set.seed(1)

mtries <- seq(1, 13)
ntrees <- seq(50, 1000, length.out = 20)

mtries

mtries.errors <- rep(NA, length(mtries))
for (i in 1:length(mtries)) {
  rf.boston <- randomForest(medv ~ ., data = Boston, 
      subset = train, mtry = mtries[i], ntree = 500)
  yhat.bag <- predict(rf.boston, newdata = Boston[-train, ])
  mtries.errors[i] = mean((yhat.bag - boston.test) ^ 2)
}

which.min(mtries.errors)


ntrees <- seq(50, 1000, length.out = 20)
ntrees.errors <- rep(NA, length(mtries))
for (i in 1:length(ntrees)) {
  rf.boston <- randomForest(medv ~ ., data = Boston, 
                            subset = train, mtry = 5, ntree = ntrees[i])
  yhat.bag <- predict(rf.boston, newdata = Boston[-train, ])
  ntrees.errors[i] = mean((yhat.bag - boston.test) ^ 2)
}

which.min(ntrees.errors)

par(mfrow = c(1, 2))
plot(mtries, mtries.errors, type = "b")
plot(ntrees, ntrees.errors, type = "b")

ntrees[5]

?randomForest

dim(Boston)

warnings()


mtries <- c(13, 6, 3)
ntrees <- seq(1, 1000, 50)
errors <- matrix(NA, length(ntrees), length(mtries), dimnames = list(ntrees, mtries))

errors

for (i in 1:length(ntrees)) {
  for (j in 1:length(mtries)) {
    rf.boston <- randomForest(medv ~ ., data = Boston, 
                              subset = train, ntree = ntrees[i], mtry = mtries[j])
    yhat.bag <- predict(rf.boston, newdata = Boston[-train, ])
    errors[i, j] = mean((yhat.bag - boston.test) ^ 2)
  }
}

par(mfrow = c(1, 1), xpd = NA)
plot(NA, NA, xlim = c(0, 1000), ylim = c(15, 50), xlab = "Number of Trees", ylab = "error")
lines(ntrees, errors[, 1], col = "red", type = "l")
lines(ntrees, errors[, 2], col = "green", type = "l")
lines(ntrees, errors[, 3], col = "blue", type = "l")
legend("topright", legend = c("m=p", "m=p/2", "m=sqrt(p)"), col = c("red", "green", "blue"), lty = "solid")

#
set.seed(1101)

train <- sample(dim(Boston)[1], dim(Boston)[1]/2)
x.train <- Boston[train, -14]
y.train <- Boston[train, 14]
x.test <- Boston[-train, -14]
y.test <- Boston[-train, 14]

p <- dim(Boston)[2] - 1
p.2 <- p / 2
p.sq <- sqrt(p)

rf.boston.p <- randomForest(x.train, y.train, xtest = x.test, ytest = y.test, mtry = p, ntree = 500)
rf.boston.p.2 <- randomForest(x.train, y.train, xtest = x.test, ytest = y.test, mtry = p.2, ntree = 500)
rf.boston.p.sq <- randomForest(x.train, y.train, xtest = x.test, ytest = y.test, mtry = p.sq, ntree = 500)

plot(1:500, rf.boston.p$test$mse, col = "green", type = "l", xlab = "Number of Trees", ylab = "Test MSE", ylim = c(10, 35))
lines(1:500, rf.boston.p.2$test$mse, col = "red", type = "l")
lines(1:500, rf.boston.p.sq$test$mse, col = "blue", type = "l")
legend("topright", c("m=p", "m=p/2", "m=sqrt(p)"), col = c("green", "red", "blue"), cex = 1, lty = 1)

# 8
set.seed(1)
train <- sample(nrow(Carseats)[1], nrow(Carseats)[1] / 2)
head(Carseats)
x.train <- Carseats[train, -1]
y.train <- Carseats[train, 1]
x.test <- Carseats[-train, -1]
y.test <- Carseats[-train, 1]

tree.carseats <- tree(Sales ~ .-High, data = Carseats, subset = train)
summary(tree.carseats)

plot(tree.carseats)
text(tree.carseats, pretty = 0)
tree.preds <- predict(tree.carseats, Carseats[-train, ])
testMSE <- mean((y.test - tree.preds) ^ 2)
testMSE

set.seed(3)
cv.carseats <- cv.tree(tree.carseats, FUN = prune.tree)
cv.carseats

par(mfrow = c(1, 2))
plot(cv.carseats$size, cv.carseats$dev, type = "b")
plot(cv.carseats$k, cv.carseats$dev, type = "b")

prune.carseats <- prune.tree(tree.carseats, best = 12)
plot(prune.carseats)
text(prune.carseats, pretty = 0)

tree.preds <- predict(prune.carseats, Carseats[-train, ])
testMSE <- mean((y.test - tree.preds) ^ 2)
testMSE

# bagging
library(randomForest)
set.seed(1)

dim(Carseats)
sqrt(10)

bag.carseats <- randomForest(Sales ~ .-High, data = Carseats, subset = train, mtry = 10, importance = TRUE)
bag.carseats

yhat.bag <- predict(bag.carseats, newdata = Carseats[-train, ])

par(mfrow = c(1, 1))
plot(yhat.bag, y.test)
abline(0, 1)
mean((yhat.bag - y.test) ^ 2)

importance(bag.carseats)

# random forests
rf.carseats <- randomForest(Sales ~ .-High, data = Carseats, subset = train, mtry = 3, importance = TRUE)
rf.carseats

yhat.rf <- predict(rf.carseats, newdata = Carseats[-train, ])

par(mfrow = c(1, 1))
plot(yhat.rf, y.test)
abline(0, 1)
mean((yhat.rf - y.test) ^ 2)


rf.carseats <- randomForest(Sales ~ .-High, data = Carseats, subset = train, 
                            mtry = 5, importance = TRUE)
rf.carseats

yhat.rf <- predict(rf.carseats, newdata = Carseats[-train, ])

par(mfrow = c(1, 1))
plot(yhat.rf, y.test)
abline(0, 1)
mean((yhat.rf - y.test) ^ 2)

importance(rf.carseats)

# 9
library(ISLR)
library(tree)

set.seed(1013)
train <- sample(dim(OJ)[1], 800)

train_set <- OJ[train, ]
test_set <- OJ[-train, ]

tree.oj <- tree(Purchase ~ ., data = train_set)
summary(tree.oj)

tree.oj

plot(tree.oj)
text(tree.oj, pretty = 0)

tree.pred <- predict(tree.oj, test_set, type = "class")
table(tree.pred, test_set$Purchase)

length(test_set$Purchase)
(47) / 270

set.seed(3)
cv.oj <- cv.tree(tree.oj, FUN = prune.misclass)
names(cv.oj)
cv.oj

par(mfrow = c(1, 2))
plot(cv.oj$size, cv.oj$dev, type = "b")
plot(cv.oj$k, cv.oj$dev, type = "b")

prune.oj <- prune.misclass(tree.oj, best = 4)
plot(prune.oj)
text(prune.oj, pretty = 0)

tree.pred <- predict(tree.oj, train_set, type = "class")
table(tree.pred, train_set$Purchase)

# 10
library(ISLR)
library(gbm)

set.seed(1)
Hitters <- na.omit(Hitters)
Hitters$Salary  <- log(Hitters$Salary)

train_set <- Hitters[1:200, ]
test_set <- Hitters[-(1:200), ]

boost.hitters <- gbm(Salary ~ ., data = train_set, distribution = "gaussian", n.trees = 1000, interaction.depth = 1)
par(mfrow = c(1, 1))
summary(boost.hitters)

boost.hitters <- gbm(Salary ~ ., data = train_set, distribution = "gaussian", n.trees = 1000, interaction.depth = 2)
par(mfrow = c(1, 1))
summary(boost.hitters)

boost.hitters <- gbm(Salary ~ ., data = train_set, distribution = "gaussian", n.trees = 1000, interaction.depth = 3)
par(mfrow = c(1, 1))
summary(boost.hitters)

boost.hitters <- gbm(Salary ~ ., data = train_set, distribution = "gaussian", n.trees = 1000, interaction.depth = 4)
par(mfrow = c(1, 1))
summary(boost.hitters)
plot(boost.hitters, i = "CHits")
plot(boost.hitters, i = "CAtBat")

shrinkage <- seq(0, 1, 0.02)

train_errors <- rep(NA, length(shrinkage))
for (i in 1:length(shrinkage)) {
  boost.hitters <- gbm(Salary ~ ., data = train_set, distribution = "gaussian", n.trees = 1000, interaction.depth = 3, shrinkage = shrinkage[i])
  yhat.boost <- predict(boost.hitters, newdata = train_set, n.trees = 1000)
  train_errors[i] <- mean((yhat.boost - train_set$Salary) ^ 2)
}

plot(shrinkage, train_errors, type = "b")

which.min(train_errors)

test_errors <- rep(NA, length(shrinkage))
for (i in 1:length(shrinkage)) {
  boost.hitters <- gbm(Salary ~ ., data = train_set, distribution = "gaussian", n.trees = 1000, interaction.depth = 3, shrinkage = shrinkage[i])
  yhat.boost <- predict(boost.hitters, newdata = test_set, n.trees = 1000)
  test_errors[i] <- mean((yhat.boost - test_set$Salary) ^ 2)
}

plot(shrinkage, test_errors, type = "b")

which.min(test_errors)

exp(test_errors[3])

shrinkage[3]

boost.hitters <- gbm(Salary ~ ., data = train_set, distribution = "gaussian", n.trees = 1000, shrinkage = shrinkage[3])
summary(boost.hitters)

yhat.boost <- predict(boost.hitters, newdata = test_set, n.trees = 1000)
mean((yhat.boost - test_set$Salary) ^ 2)

# bagging
library(randomForest)
set.seed(1)

bag.hitters <- randomForest(Salary ~ ., data = train_set, mtry = 19, importance = TRUE)
bag.hitters

yhat.bag <- predict(bag.hitters, newdata = test_set)
plot(yhat.bag, test_set$Salary)
abline(0, 1)
exp(mean((yhat.bag - test_set$Salary) ^ 2))

set.seed(1)
bag.hitters <- randomForest(Salary ~ ., data = train_set, mtry = 6, importance = TRUE)
bag.hitters

yhat.bag <- predict(bag.hitters, newdata = test_set)
plot(yhat.bag, test_set$Salary)
abline(0, 1)
mean((yhat.bag - test_set$Salary) ^ 2)

# 11
library(ISLR)
library(gbm)

set.seed(342)

Caravan$Purchase <- ifelse(Caravan$Purchase == "Yes", 1, 0)
train <- sample(dim(Caravan)[1], 1000)
train_set <- Caravan[train, ]
test_set <- Caravan[-train, ]

head(Caravan)
boost.caravan <- gbm(Purchase ~ ., data = train_set, distribution = "bernoulli", n.trees = 1000, shrinkage = 0.01)
summary(boost.caravan)

par(mfrow = c(1, 2))
plot(boost.caravan, i = "MOSHOOFD")

yhat.boost <- predict(boost.caravan, newdata = test_set, n.trees = 1000)
yhat.probs <- rep("No", nrow(test_set))
yhat.probs[yhat.boost > .2] <- "Yes"
table(yhat.probs, test_set$Purchase)

# 12
library(ISLR)
library(MASS)
library(gbm)

set.seed(141)
head(Smarket)

Smarket$Direction <- ifelse(Smarket$Direction == "Up", 1, 0)
train <- sample(dim(Smarket)[1], 800)
train_set <- Smarket[train, ]
test_set <- Smarket[-train, ]

# boosting
shrinkages <- 10 ^ seq(-10, -0.2, by = 0.1)
errors <- rep(NA, length(shrinkages))

for (i in 1:length(shrinkages)) {
  boost.smarket <- gbm(Direction ~ .-Today - Year, train_set, distribution = "bernoulli", n.trees = 5000, shrinkage = shrinkages[i])
  yhat.boost <- predict(boost.smarket, test_set, n.trees = 5000)
  yhat.probs <- rep(0, nrow(test_set))
  yhat.probs[yhat.boost > .5] <- 1
  errors[i] <- 1 - (sum(diag(table(yhat.probs, test_set$Direction))) / nrow(test_set))
}

par(mfrow = c(1, 1))
plot(shrinkages, errors, type = "b")
which.min(errors)
points(shrinkages[which.min(errors)], errors[which.min(errors)], col = "red", pch = 2)

shrinkages[which.min(errors)]
errors[which.min(errors)]
table(yhat.probs, test_set$Direction)

(12 + 222) / nrow(test_set)

# bagging
library(randomForest)
library(ISLR)
library(MASS)

set.seed(141)
head(Smarket)

train <- sample(dim(Smarket)[1], 800)
train_set <- Smarket[train, ]
test_set <- Smarket[-train, ]

bag.smarket <- randomForest(Direction ~ . -Today, data = train_set, mtry = 7, importance = TRUE)
bag.smarket

bag.preds <- predict(bag.smarket, newdata = test_set)
error <- 1 - (sum(diag(table(bag.preds, test_set$Direction))) / nrow(test_set))
error

# random forest
library(randomForest)
library(ISLR)
library(MASS)

set.seed(141)
head(Smarket)

# p / 2
bag.smarket <- randomForest(Direction ~ . -Today, data = train_set, mtry = 7 / 2, importance = TRUE)
bag.smarket

bag.preds <- predict(bag.smarket, newdata = test_set)
error <- 1 - (sum(diag(table(bag.preds, test_set$Direction))) / nrow(test_set))
error

# sqrt(p)
bag.smarket <- randomForest(Direction ~ . -Today, data = train_set, mtry = sqrt(7), importance = TRUE)
bag.smarket

bag.preds <- predict(bag.smarket, newdata = test_set)
error <- 1 - (sum(diag(table(bag.preds, test_set$Direction))) / nrow(test_set))
error

# logistic regression
library(glm)
glm.fits <- glm(Direction ~ . - Today, data = train_set, family = binomial)
summary(glm.fits)

glm.probs <- predict(glm.fits, test_set, type = "response")
glm.preds <- rep("No", nrow(test_set))
glm.preds[glm.probs > .5] <- "Yes"
error <- 1 - (sum(diag(table(bag.preds, test_set$Direction))) / nrow(test_set))
error
