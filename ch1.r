roll <- function() {
  die <- 1:6
  probs <- c(1/8, 1/8, 1/8, 1/8, 1/8, 3/8)
  dice <- sample(die, size = 2, replace = TRUE, prob = probs)
  sum(dice)
}

library(ggplot2)

x <- c(-1, -0.8, -0.6, -0.4, -0.2, 0, 0.2, 0.4, 0.6, 0.8, 1)
y <- x^3
qplot(x, y)
x <- c(1, 2, 2, 2, 3, 3)
qplot(x, binwidth = 1)

x2 <- c(1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 4)
qplot(x2, binwidth = 1)

x3 <- c(0, 1, 1, 2, 2, 2, 3, 3, 4)
qplot(x3, binwidth = 1)

replicate(3, 1 + 1)

rolls <- replicate(10000, roll())
qplot(rolls, binwidth = 1)

die <- c(1, 2, 3, 4, 5, 6)
die
is.vector(die)
five <- 5
five
is.vector(five)
length(five)
length(die)
int <- 1L
text <- 'ace'
text
num <- c(1L, 5L)
int
text <- c("ace", "hearts")
text
sum(num)
sum(text)
typeof(die)

int <- c(-1L, 2L, 4L)
int
typeof(int)
sqrt(2) ^ 2 - 2

text <- c("Hello", "World")
text
typeof(text)
typeof("hello")
typeof(TRUE)
logic <- c(TRUE, FALSE, TRUE)
typeof(logic)

comp <- c(1 + 1i, 1 + 2i, 1 + 3i)
comp
typeof(comp)
raw(3)
typeof(raw(3))

hand <- c("ace", "king", "queen", "jack", "ten")
hand
typeof(hand)

attributes(die)
names(die)

names(die) <- c("one", "two", "three", "four", "five", "six")
names(die)

attributes(die)
die
die + 1
names(die) <- c("uno", "dos", "tres", "quatro", "cinco", "seis")
die
names(die) <- NULL
die

dim(die) <- c(2, 3)
die

dim(die) <- c(3, 2)
die

dim(die) <- c(1, 2, 3)
die

m <- matrix(die, nrow=2, byrow = TRUE)
m

?matrix


c(11:14, 21:24, 31:34)
ar <- array(c(11:14, 21:24, 31:34), dim = c(2, 2, 3))
ar

hand1 <- c("ace", "king", "queen", "jack", "ten", "spades", "spades", "spades", "spades", "spades")

matrix(hand1, nrow = 5)
matrix(hand1, ncol = 2)
dim(hand1) <- c(5, 2)
hand1

hand2 <- c("ace", "spades", "king", "spades", "queen", "spades", "jack",
           "spades", "ten", "spades")
matrix(hand2, nrow=5, byrow=TRUE)
matrix(hand2, ncol=2, byrow=TRUE)

dim(die) <- c(2, 3)
typeof(die)

class(die)
attributes(die)

class("Hello")
class(5)

now < Sys.time()
now

typeof(now)
class(now)

unclass(now)

mil <- 100000
mil

class(mil) <- c("POSIXct", "POSIXt")
mil

gender <- factor(c("male", "female", "female", "male"))
typeof(gender)
attributes(gender)
gender
unclass(gender)

as.character(gender)

card <- c("ace", "hearts", 1)
card
sum(c(TRUE, TRUE, FALSE, FALSE))
mean(c(TRUE, TRUE, FALSE, FALSE))

list1 <- list(100:130, "R", list(TRUE, FALSE))
list1

card <- list("ace", "hearts", 1)
card

df <- data.frame(face = c("ace"), suit = c("clubs", "clubs", "clubs"), value = c(1, 2, 3), stringsAsFactors = FALSE)
df

typeof(df)
class(df)
str(df)

l <- list(face = "ace", suit = "hearts", value = 1)
l
names(l)

c <- c(face = "ace", suit = "hearts", value = "one")
c
names(c)

deck <- data.frame(
  face = c("king", "queen", "jack", "ten", "nine", "eight", "seven", "six",
           "five", "four", "three", "two", "ace", "king", "queen", "jack", "ten",
           "nine", "eight", "seven", "six", "five", "four", "three", "two", "ace",
           "king", "queen", "jack", "ten", "nine", "eight", "seven", "six", "five",
           "four", "three", "two", "ace", "king", "queen", "jack", "ten", "nine",
           "eight", "seven", "six", "five", "four", "three", "two", "ace"),
  suit = c("spades", "spades", "spades", "spades", "spades", "spades",
           "spades", "spades", "spades", "spades", "spades", "spades", "spades",
           "clubs", "clubs", "clubs", "clubs", "clubs", "clubs", "clubs", "clubs",
           "clubs", "clubs", "clubs", "clubs", "clubs", "diamonds", "diamonds",
           "diamonds", "diamonds", "diamonds", "diamonds", "diamonds", "diamonds",
           "diamonds", "diamonds", "diamonds", "diamonds", "diamonds", "hearts",
           "hearts", "hearts", "hearts", "hearts", "hearts", "hearts", "hearts",
           "hearts", "hearts", "hearts", "hearts", "hearts"),
  value = c(13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 13, 12, 11, 10, 9, 8,
            7, 6, 5, 4, 3, 2, 1, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 13, 12, 11,
            10, 9, 8, 7, 6, 5, 4, 3, 2, 1)
)

deck

view(deck)

head(deck)
deck[1, 1]
deck[1, c(1, 2, 3)]
class(deck[c(1, 1), c(1, 2, 3)])

deck[c(1, 1), c(1, 2, 3)]
vec <- c(6, 1, 3, 6, 10, 5)
vec[1:3]
deck[1:2, 1:2]
deck[1:2, 1]
deck[1:2, 1, drop = FALSE]

deck[-1, 1:3]
deck[-(2:52), 1:3]
deck[c(-1, 1), 1]
deck[0, 0]
deck[1, ]

deck[1:8, c(TRUE, TRUE, FALSE)]

rows <- c(TRUE, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F,
          F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F,
          F, F, F, F, F, F, F, F, F, F, F, F, F, F)
deck[rows, ]
deck[1, c("face", "suit", "value")]
deck[ , "value"]

deal <- function(cards) {
  cards[1, ]
}

deal(deck)
deck2 <- deck[1:52, ]
head(deck2)

deck3 <- deck[c(2, 1, 3:52), ]
head(deck3)

random <- sample(1:52, size = 52)
random

deck4 <- deck[random, ]
head(deck4)

shuffle <- function(cards) {
  random <- sample(1:52, size = 52)
  cards[random, ]
}

deal(deck)
deck2 <- shuffle(deck)
deal(shuffle(deck))

deck$value

mean(deck$value)
median(deck$value)

lst <- list(numbers = c(1, 2), logical = TRUE, strings = c("a", "b", "c"))
lst
lst[1]

sum(lst[1])
lst$numbers
sum(lst$numbers)
lst[[1]]

lst["numbers"]
lst[["numbers"]]

deck2 <- deck

vec <- c(0, 0, 0, 0, 0, 0)
vec
vec[1]
vec[1] <- 1000
vec
vec[c(1, 3, 5)] <- c(1, 1, 1)
vec
vec[4:6] <- vec[4:6] + 1
vec
vec[7] <- 0
vec
deck2$new <- 1:52
head(deck2)

deck2$new <- NULL
head(deck2)

deck2[c(13, 26, 39, 52), 3]
deck2[c(13, 26, 39, 52), ]$value <- 14

deck2[c(13, 26, 39, 52), ]
head(deck2, 13)

deck3 <- shuffle(deck)
head(deck3)

sum(deck2$face == "ace")

deck3$value[deck3$face == "ace"] <- 14
head(deck3, 13)

deck4 <- deck
deck4$value <- 0

deck4$value[deck4$suit == "hearts"] <- 1
deck4
head(deck4, 13)

deck4[deck4$face == "queen", ]
deck4[deck4$suit == "spades", ]

a <- c(1, 2, 3)
b <- c(1, 2, 3)
c <- c(1, 2, 4)

a == b
b == c
a == b & b == c

queenOfSpades <- deck4$face == "queen" & deck4$suit == "spades"
deck4[queenOfSpades, ]
deck4$value[queenOfSpades] <- 13

w <- c(-1, 0, 1)
x <- c(5, 15)
y <- "February"
z <- c("Monday", "Tuesday", "Friday")

w > 0
x > 10 & x < 20
y == "February"
all(z %in% c("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"))

deck5 <- deck
head(deck5, 13)

facecard <- deck5$face %in% c("king", "queen", "jack")
deck5[facecard, ]
deck5[facecard, ] <- 10
head(deck5, 13)


deck5$value[deck5$face == "ace"] <- NA
head(deck5, 13)

deal(deck)

library(pryr)
parenvs(all = TRUE)

as.environment("package:stats")
globalenv()
baseenv()
emptyenv()
parent.env(globalenv())
parent.env(emptyenv())
ls(emptyenv())
ls(globalenv())
ls(baseenv())

ls.str(globalenv())
head(globalenv()$deck, 3)
assign("message", "Hello Global", envir = globalenv())
globalenv()$message


environment()
assign("new", "Hello Global", envir = globalenv())
new
new <- "Hello Active"
new

foo <- "take me to your runtime"

show_env <- function(x = foo) {
  list(ran.in = environment(), 
       parent = parent.env(environment()),
       objects = ls.str(environment()))
}
show_env()

environment(show_env)
environment(parenvs)

deal <- function() {
  deck[1, ]
}
environment(deal)
deal()

DECK <- deck
deck <- deck[-1, ]
head(deck, 3)

deal <- function() {
  card <- deck[1, ]
  assign("deck", deck[-1, ], envir = globalenv())
  card
}

deal()
deal()

shuffle <- function(cards) {
  random <- sample(1:52, size = 52)
  cards[random, ]
}

head(deck, 3)

a <- shuffle(deck)

head(deck, 3)
head(a, 3)

shuffle <- function() {
  random <- sample(1:52, size = 52)
  assign("deck", DECK[random, ], envir = globalenv())
}

shuffle()
head(deck, 3)

deal()
deal()
deal()

deck

setup <- function(deck) {
  DECK <- deck
  
  DEAL <- function() {
    card <- deck[1, ]
    assign("deck", deck[-1, ], envir = parent.env(environment()))
    card
  }
  
  SHUFFLE <- function() {
    random <- sample(1:52, size = 52)
    assign("deck", DECK[random, ], envir = parent.env(environment()))
  }
  
  list(deal = DEAL, shuffle = SHUFFLE)
}

cards <- setup(deck)

deal <- cards$deal
shuffle <- cards$shuffle

deal
shuffle

environment(deal)
environment(shuffle)

rm(deck)

shuffle()
deal()
deal()

get_symbols <- function() {
  wheel <- c("DD", "7", "BBB", "BB", "B", "C", "0")
  sample(wheel, size = 3, replace = TRUE,
         prob = c(0.03, 0.03, 0.06, 0.1, 0.25, 0.01, 0.52))
}

get_symbols()
