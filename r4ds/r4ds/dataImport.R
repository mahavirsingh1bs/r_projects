library(tidyverse)

heights <- read_csv("data/heights.csv")

read_csv("a,b,c
  1,2,3
  4,5,6")

read_csv("The first line of metadata
         The second line of metadata
         x,y,z
         1,2,3", skip = 2)

read_csv("# A comment I want to skip
         x,y,z
         1,2,3", comment = "#")

read_csv("1,2,3\n4,5,6", col_names = FALSE)

read_csv("a,b,c\n1,2,\"NA\"", quoted_na = FALSE)

?read_fwf

?fread
?read_csv
?read_tsv

union(names(formals(read_csv)), names(formals(read_tsv)))

?formals

read_csv("x,y\n1,'a,b'", quote = "'")

?read_delim

read_csv("a,b\n1,2,3\n4,5,6")
read_csv("a,b,c\n1,2\n1,2,3,4")
read_csv("a,b\n\"1")
read_csv("a,b\n1,2\na,b")
read_csv("a;b\n1;3")
read_csv2("a;b\n1;3")

str(parse_logical(c("TRUE", "FALSE", "NA")))
str(parse_integer(c("1", "2", "3")))
str(parse_date(c("2010-01-01", "1979-10-14")))
parse_integer(c("1", "231", ".", "456"), na = ".")
x <- parse_integer(c("123", "345", "abc", "123.45"))
x
problems(x)


# Parsing a vector --------------------------------------------------------

# Numbers -----------------------------------------------------------------

parse_double("1.24")
parse_double("1,24", locale = locale(decimal_mark = ","))
parse_number("$100")
parse_number("20%")

parse_number("$123,456,789")

parse_number(
  "123.456.789",
  locale = locale(decimal_mark = ".")
)

parse_number(
  "3.456,899.78",
  locale = locale(grouping_mark = ".", decimal_mark = ",")
)

charToRaw("Hadley")

x1 <- "El Ni\xf1o was particularly bad this year"
x2 <- "\x82\xb1\x82\xf1\x82\xc9\x82\xbf\x82\xcd"
parse_character(x1, locale = locale(encoding = "Latin1"))
parse_character(x2, locale = locale(encoding = "Shift-JIS"))
x2

guess_encoding(charToRaw(x1))
guess_encoding(charToRaw(x2))

fruit <- c("apple", "banana")
parse_factor(c("apple", "banana", "bananana"), levels = fruit)

locale(grouping_mark = ".")
parse_datetime("2010-10-01T2010")
parse_datetime("20101010")

parse_date("2010/10/01")

library(hms)
parse_time("01:10 am")
parse_time("20:10:01")

parse_date("1 janvier 2015", "%d %B %Y", locale = locale("fr"))

locale(decimal_mark = ",")

loc <- locale(date_format = "%m/%d/%y", time_format = "%H/%M/%S")

parse_date("01/01/2010", locale = loc)

?tim

?stringi::stri_enc_detect

d1 <- "January 1, 2010"
d2 <- "2015-Mar-07"
d3 <- "06-Jun-2017"
d4 <- c("August 19 (2015)", "July 1 (2015)")
d5 <- "12/30/14" # Dec 30, 2014
t1 <- "1705"
t2 <- "11:15:10.12 PM"

parse_time(t2, "%H:%M:%OS %p")

guess_parser("2010-10-01")
guess_parser("1501")

challenge <- read_csv(readr_example("challenge.csv"))
problems(challenge)

challenge <- read_csv(
  readr_example("challenge.csv"),
  col_types = cols(
    x = col_double(),
    y = col_date()
  )
)
problems(challenge)
tail(challenge)

?stop_for_problems

challenge2 <- read_csv(
  readr_example("challenge.csv"),
  guess_max = 1001
)
challenge2

challenge2 <- read_csv(
  readr_example("challenge.csv"),
  col_types = cols(.default = col_character())
)

df <- tribble(
  ~x, ~y,
  "1", "1.21",
  "2", "2.32",
  "3", "4.56"
)
df

type_convert(df)

lines <- read_lines(readr_example("challenge.csv"))
lines

typeof(lines)

write_csv(challenge, "challenge.csv", append = TRUE)

?cols

write_rds(challenge, "challenge.rds")
read_rds("challenge.rds")

