df <- tibble(
  a = rnorm(10),
  b = rnorm(10),
  c = rnorm(10),
  d = rnorm(10)
)

x <- df$a

rng <- range(x, na.rm = TRUE)
(x - rng[1]) / (rng[2] - rng[1])

(df$a - min(df$a, na.rm = TRUE)) /
  (max(df$a, na.rm = TRUE) - min(df$a, na.rm = TRUE))

rescale01 <- function(x) {
  rng <- range(x, na.rm = TRUE, finite = TRUE)
  y <- (x - rng[1]) / (rng[2] - rng[1])
  y[y == Inf] <- 1
  y[y == -Inf] <- 0
  y
}

rescale01 <- function(x) {
  x[x == Inf] <- 1
  x[x == -Inf] <- 0
  rng <- range(x, na.rm = TRUE, finite = TRUE)
  (x - rng[1]) / (rng[2] - rng[1])
}

rescale01(c(Inf, -Inf, 0:5, NA))

rescale01(c(-10, 0, 10))

x <- c(1:10, Inf)
rescale01(x)

x <- c(1:10, NA, NA)

mean(is.na(x))

prop_na <- function(x) {
  mean(is.na(x))
}

prop_na(x)

sum_to_one <- function(x, na.rm = FALSE) {
  x / sum(x, na.rm = na.rm)
}

sum_to_one(c(1:5, NA), na.rm = TRUE)

sd(x, na.rm = TRUE) / mean(x, na.rm = TRUE)

coef_variation <- function(x, na.rm = FALSE) {
  sd(x, na.rm = na.rm) / mean(x, na.rm = na.rm)
}

coef_variation(1:5)
coef_variation(c(1:5, NA), na.rm = TRUE)

n <- length(df$a)
v <- var(df$a)
m <- mean(df$a)

third.moment <- (1 / (n - 2)) * sum((df$a - m) ^ 3) 
third.moment / (v ^ (3/2))

n <- length(df$a)
(1 / (n - 1))

m <- mean(df$a)
df$a - m
sum((df$a - m) ^ 2)
(1 / (n - 1)) * sum((df$a - m) ^ 2)
var(df$a)

variance <- function(x, na.rm = FALSE) {
  n <- length(x)
  m <- mean(x, na.rm = na.rm)
  sq_err <- (x - m) ^ 2
  sum(sq_err) / (n - 1)
}

variance(1:10)
var(1:10)

skewness <- function(x, na.rm = FALSE) {
  n <- length(x)
  m <- mean(x, na.rm = na.rm)
  v <- var(x, na.rm = na.rm)
  
  (sum(x - m) ^ 3 / (n - 2)) / v ^ (3 / 2)
}

skewness(c(1, 5, 10, 100))


x <- c(1, 2, 3, NA, 4, NA, NA, NA, NA, NA)
y <- c(1, 2, 3, 7, 4, NA, 5, NA, 6, 8)
sum(is.na(x[is.na(x)] == y[is.na(y)]))

is.na(x)
is.na(y)

is.na(x) & is.na(y)

length(x)
length(y)

is.na(y)

is.na(x) & is.na(y)

xNAs <- c(FALSE, TRUE, TRUE, TRUE, FALSE)
yNAs <- c(TRUE, TRUE, TRUE, TRUE, FALSE)

xNAs & yNAs

both_na <- function(x, y) {
  sum(is.na(x) & is.na(y))
}

both_na(x, y)

both_na(
  c(NA, NA, 1, 2),
  c(NA, 1, NA, 2)
)

foo_foo <- "Little bunny Foo Foo"

foo <- function(bunny) {
  str_c(bunny, "Foo", sep = " ")
}

hop <- function (foo_foo, through) {
  str_c(foo_foo, str_c("Went hopping through the", through, sep = " "), sep = "\n")
}

scoop <- function(foo_foo, up) {
  str_c(foo_foo, str_c("Scooping up the", up, sep = " "), sep = "\n")
}

bop <- function(foo_foo, on) {
  str_c(foo_foo, str_c("And bopping them on the", on, sep = " "), sep = "\n")
}

rescale01_alt2 <- function(x, na.rm = FALSE, finite = FALSE) {
  rng <- range(x, na.rm = na.rm, finite = finite)
  print(rng)
  (x - rng[1]) / (rng[2] - rng[1])
}

rescale01_alt2(c(NA, 1:5), na.rm = FALSE, finite = FALSE)

bunny = "Little bunny"

forest <- "forest"
field_mouse <- "field mouse"
head <- "head"

bunny %>%
  foo() %>% 
  foo() %>% 
  hop(through = forest) %>%
  scoop(up = field_mouse) %>%
  bop(on = head) %>% 
  writeLines()


threat <- function(chances) {
  give_chances(
    from = Good_Fairy,
    to = foo_foo,
    number = chances,
    condition = "Don't behave",
    consequence = turn_into_goon
  )
}

lyric <- function() {
  foo_foo %>%
    hop(through = forest) %>%
    scoop(up = field_mouse) %>%
    bop(on = head)
  
  down_came(Good_Fairy)
  said(
    Good_Fairy,
    c(
      "Little bunny Foo Foo",
      "I don't want to see you",
      "Scooping up the field mice",
      "And bopping them on the head."
    )
  )
}

lyric()
threat(3)
lyric()
threat(2)
lyric()
threat(1)
lyric()
turn_into_goon(Good_Fairy, foo_foo)

# Functions ---------------------------------------------------------------

has_prefix <- function(str, prefix) {
  substr(str, 1, nchar(prefix)) == prefix
}

has_prefix(c("ab", "abc", "abcd"), "abc")

drop_last <- function(x) {
  if (length(x) <= 1) return(NULL)
  x[-length(x)]
}

drop_last(1:3)
drop_last(1:2)
drop_last(1)

recycle <- function(x, y) {
  rep(y, length.out = length(x))
}

expand(1:4, 5)
?expand

has_name <- function(x) {
  nms <- names(x)
  if (is.null(nms)) {
    rep(FALSE, length(x))
  } else {
    !is.na(nms) & nms != ""
  }
}

x <- c(name = "Name", value = "Value", "Value1")
has_name(x)

x1 <- c("Name", value = "Value", "Value1")
has_name(x1)

names(x1)

if (c(TRUE, "ab")) { print("Hello") }
x <- sqrt(2) ^ 2
x
x == 2

?cut

?ifelse
??test

x <- c(1:10)
if (x <= 1) {
  print("True")
}

ifelse(x >= 5, "yes", "no")

ifelse(x >= 5, sum(x), mean(x))

ifelse(x >= 5, sum(x), mean(x))

greet <- function(time = now()) {
  if (hour(time) >= 6 && hour(time) <= 12) {
    print("Good morning")
  } else if (hour(time) > 12 & hour(time) <= 18) {
    print("Good evening")
  } else {
    print("Good night")
  }
}

print(now() + hours(6))
greet(now() + hours(6))

num <- 88

if (num %% 3 == 0) {
  print("fizz")
} else if (num %% 5 == 0) {
  print("buzz")
} else if (num %% 3 == 0 && num %% 5 == 0) {
  print("fizzbuzz")
} else {
  print(num)
}

fizzbuzz <- function(num) {
  if (num %% 3 == 0) {
    return("fizz")
  } else if (num %% 5 == 0) {
    return("buzz")
  } else if (num %% 3 == 0 && num %% 5 == 0) {
    return("fizzbuzz")
  } else {
    return(num)
  }  
}
fizzbuzz(30)

?cut

temp <- 10

if (temp <= 0) {
  "freezing"
} else if (temp <= 10) {
  "cold"
} else if (temp <= 20) {
  "cool"
} else if (temp <= 30) {
  "warm"
} else {
  "hot"
}


temp <- seq(-10, 50, by = 5)
temp

cut(temp, 
  c(-Inf, 0, 10, 20, 30, Inf),
  right = TRUE,
  labels = 
)


levels <- levels(cuts)
levels

if (temp <= 0) {
  "freezing"
} else if (cuts == levels[1]) {
  "cold"
} else if (cuts == levels[2]) {
  "cool"
} else if (cuts == levels[3]) {
  "warm"
} else {
  "hot"
}

?cut

switch (cuts,
    levels[0] = "cold",
    levels[1] = "cool",
    levels[2] = "warm",
    stop("No match")
)

switch(1, "apple", "banana", "cantaloupe")

oper <- function(x, y, op) {
  switch(op,
         plus = x + y,
         minus = x - y,
         times = x * y,
         divide = x / y,
         stop("Unknown op!")
  )
}
oper(5, 8, "times")


x <- "e"
switch(x,
       a = ,
       b = "ab",
       c = ,
       d = "cd"
)

?switch

switch(x,
       NULL, 
       a = "ab",
       b = "ab",
       c = "cd",
       d = "cd" # value to return if x not matched
)

wt_mean <- function(x, w, na.rm = FALSE) {
  stopifnot(is.logical(na.rm), length(na.rm) == 1)
  stopifnot(length(x) == length(w))
  
  if (na.rm) {
    miss <- is.na(x) | is.na(w)
    x <- x[!miss]
    w <- w[!miss]
  }
  sum(w * x) / sum(x)
}
wt_var <- function(x, w) {
  mu <- wt_mean(x, w)
  sum(w * (x - mu) ^ 2) / sum(w)
}
wt_sd <- function(x, w) {
  sqrt(wt_var(x, w))
}

wt_mean(1:6, 1:3)

commas <- function(...) stringr::str_c(..., collapse = ", ")
commas(letters[1:10])

commas(letters, collapse = "-")

rule <- function(..., pad = "-") {
  title <- paste0(...)
  width <- getOption("width") - nchar(title) - 5
  cat(title, " ", stringr::str_dup(pad, width), "\n", sep = "")
}
rule("Important output", pad = "-+")

?cor

rule <- function(..., pad = "-") {
  title <- paste0(...)
  width <- getOption("width") - nchar(title) - 5
  padding <- stringr::str_dup(
    pad,
    ceiling(width / stringr::str_length(title))
  ) %>%
    stringr::str_trunc(width)
  cat(title, " ", padding, "\n", sep = "")
}
rule("Important output")

?mean

`+` <- function(x, y) {
  if (runif(1) < 0.1) {
    sum(x, y)
  } else {
    sum(x, y) * 1.1
  }
}
table(replicate(1000, 1 + 2))

replicate(1000, 1 + 2)

?runif
runif(1)
