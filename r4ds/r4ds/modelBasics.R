library(tidyverse)

library(modelr)
options(na.action = na.warn)

# Section 8.1. Simple Model -----------------------------------------------

ggplot(sim1, aes(x, y)) + 
  geom_point()

models <- tibble(
  a1 = runif(250, -20, 40),
  a2 = runif(250, -5, 5)
)

ggplot(sim1, aes(x, y)) + 
  geom_point() + 
  geom_abline(
    aes(intercept = a1, slope = a2),
    data = models, alpha = 1/4
  )

model1 <- function(a, data) {
  a[1] + data$x * a[2]
}

model1(c(7, 1.5), sim1)

measure_distance <- function(mod, data) {
  diff <- data$y - model1(mod, data)
  sqrt(mean(diff ^ 2))
}
measure_distance(c(7, 1.5), sim1)

sim1_dist <- function(a1, a2) {
  measure_distance(c(a1, a2), sim1)
}

models <- models %>% 
  mutate(dist = purrr::map2_dbl(a1, a2, sim1_dist))
models

ggplot(sim1, aes(x, y)) + 
  geom_point(size = 2, color = "grey30") + 
  geom_abline(
    aes(intercept = a1, slope = a2, color = -dist),
    data = filter(models, rank(dist) <= 10)
  )

ggplot(models, aes(a1, a2)) + 
  geom_point(data = filter(models, rank(dist) <= 10), 
    size = 4, color = "red"
  ) +
  geom_point(aes(color = -dist))

filter(models, rank(dist) <= 10) %>% 
  mutate(min_a1 = min(a1), max_a1 = max(a1), min_a2 = min(a2), max_a2 = max(a2))

grid <- expand.grid(
  a1 = seq(-0.4, 13, length = 25),
  a2 = seq(1, 4, length = 25)
) %>% 
  mutate(dist = purrr::map2_dbl(a1, a2, sim1_dist))

grid %>% 
  ggplot(aes(a1, a2)) + 
  geom_point(
    data = filter(grid, rank(dist) <= 10),
    size = 4, color = "red"
  ) +
  geom_point(aes(color = -dist))

ggplot(sim1, aes(x, y)) +
  geom_point(size = 2, color = "grey30") +
  geom_abline(
    aes(intercept = a1, slope = a2, color = -dist),
    data = filter(grid, rank(dist) <= 1)
  )

filter(grid, rank(dist) <= 1)

best <- optim(c(0, 0), measure_distance, data = sim1)
best$par

ggplot(sim1, aes(x, y)) + 
  geom_point(size = 2, color = "grey30") + 
  geom_abline(
    aes(intercept = best$par[1], slope = best$par[2])
  )

sim1_mod <- lm(y ~ x, data = sim1)
coef(sim1_mod)


# Exercise ----------------------------------------------------------------

# 1
sim1a <- tibble(
  x = rep(1:10, each = 3),
  y = x * 1.5 + 6 + rt(length(x), df = 2)
)

best1a <- optim(c(0, 0), measure_distance, data = sim1a)
best1a$par

ggplot(sim1a, aes(x, y)) + 
  geom_point(size = 2, color = "grey30") + 
  geom_abline(
    aes(intercept = best1a$par[1], slope = best1a$par[2])
  )

# 2
make_prediction <- function(mod, data) {
  mod[1] + mod[2] * data$x
}

measure_distance <- function(mod, data) {
  diff <- data$y - make_prediction(mod, data)
  mean(abs(diff))
}

best1a <- optim(c(0, 0), measure_distance, data = sim1a)
best1a$par

ggplot(sim1a, aes(x, y)) + 
  geom_point(size = 2, color = "grey30") + 
  geom_abline(
    aes(intercept = best1a$par[1], slope = best1a$par[2])
  )

# Section 8.2. Visualizing Models -----------------------------------------

grid <- sim1 %>% 
  data_grid(x)
grid

grid <- grid %>% 
  add_predictions(sim1_mod)

ggplot(sim1, aes(x)) + 
  geom_point(aes(y = y)) + 
  geom_line(
    aes(y = pred), 
    data = grid, 
    color = "red",
    size = 1
  )

sim1 <- sim1 %>% 
  add_residuals(sim1_mod)

ggplot(sim1, aes(resid)) + 
  geom_freqpoly(binwidth = 0.5)

ggplot(sim1, aes(x, resid)) + 
  geom_ref_line(h = 0) + 
  geom_point()

# Exercise ----------------------------------------------------------------

sim1_loess <- loess(y ~ x, data = sim1)

grid <- sim1 %>% 
  data_grid(x)
grid

grid <- grid %>% 
  add_predictions(sim1_loess)

ggplot(sim1, aes(x)) + 
  geom_point(aes(y = y)) + 
  geom_line(
    aes(y = pred), 
    data = grid, 
    color = "red",
    size = 1
  )

sim1 <- sim1 %>% 
  add_residuals(sim1_loess)

ggplot(sim1, aes(resid)) + 
  geom_freqpoly(binwidth = 0.5)

ggplot(sim1, aes(x, resid)) + 
  geom_ref_line(h = 0) + 
  geom_point()

# Section 8.3. Formulas and Model Families --------------------------------

df <- tribble(
  ~y, ~x1, ~x2,
  4, 2, 5,
  5, 1, 6
)
model_matrix(df, y ~ x1)
model_matrix(df, y ~ x1 - 1)
model_matrix(df, y ~ x1 + x2)

df <- tribble(
  ~ sex, ~ response,
  "male", 1,
  "female", 2,
  "male", 1
)
model_matrix(df, response ~ sex)

ggplot(sim2, aes(x, y)) + 
  geom_point()

mod2 <- lm(y ~ x, data = sim2)

grid <- sim2 %>% 
  data_grid(x) %>% 
  add_predictions(mod2)
grid

ggplot(sim2, aes(x)) + 
  geom_point(aes(y = y)) + 
  geom_point(
    data = grid,
    aes(y = pred),
    color = "red",
    size = 4
  )

tibble(x = "e") %>% 
  add_predictions(mod2)

# Interactions ------------------------------------------------------------

ggplot(sim3, aes(x1, y)) + 
  geom_point(aes(color = x2))

mod1 <- lm(y ~ x1 + x2, data = sim3)
mod2 <- lm(y ~ x1 * x2, data = sim3)

grid <- sim3 %>% 
  data_grid(x1, x2) %>% 
  gather_predictions(mod1, mod2)
grid

ggplot(sim3, aes(x1, y, color = x2)) + 
  geom_point() + 
  geom_line(data = grid, aes(y = pred)) + 
  facet_wrap(~ model)

grid %>% 
  filter(x2 == "d")

sim3 <- sim3 %>% 
  gather_residuals(mod1, mod2)

sim3

ggplot(sim3, aes(x1, resid, color = x2)) +
  geom_point() + 
  facet_grid(model ~ x2)

# Interactions (Two continuous) -------------------------------------------

mod1 <- lm(y ~ x1 + x2, data = sim4)
mod2 <- lm(y ~ x1 * x2, data = sim4)

grid <- sim4 %>% 
  data_grid(
    x1 = seq_range(x1, 5),
    x2 = seq_range(x2, 5)
  ) %>% 
  gather_predictions(mod1, mod2)
grid

ggplot(grid, aes(x1, x2)) + 
  geom_tile(aes(fill = pred)) + 
  facet_wrap(~ model)

ggplot(grid, aes(x1, pred, color = x2, group = x2)) +
  geom_line() +
  facet_wrap(~ model)

ggplot(grid, aes(x2, pred, color = x1, group = x1)) +
  geom_line() +
  facet_wrap(~ model)

# Section 8.4. Transformations --------------------------------------------

df <- tribble(
  ~y, ~x,
  1,  1,
  2,  2,
  3,  3
)

model_matrix(df, y ~ x^2 + x)
model_matrix(df, y ~ I(x^2) + x)
model_matrix(df, y ~ poly(x, 2))

library(splines)
model_matrix(df, y ~ ns(x, 2))

sim5 <- tibble(
  x = seq(0, 3.5 * pi, length = 50),
  y = 4 * sin(x) + rnorm(length(x))
)
sim5

ggplot(sim5, aes(x, y)) + 
  geom_point()

mod1 <- lm(y ~ ns(x, 1), data = sim5)
mod2 <- lm(y ~ ns(x, 2), data = sim5)
mod3 <- lm(y ~ ns(x, 3), data = sim5)
mod4 <- lm(y ~ ns(x, 4), data = sim5)
mod5 <- lm(y ~ ns(x, 5), data = sim5)

grid <- sim5 %>% 
  data_grid(x = seq_range(x, n = 50, expand = 0.1)) %>% 
  gather_predictions(mod1, mod2, mod3, mod4, mod5, .pred = "y")
grid

ggplot(sim5, aes(x, y)) + 
  geom_point() + 
  geom_line(data = grid, color = "red") + 
  facet_wrap(~ model)


# Exercise ----------------------------------------------------------------

mod1 <- lm(y ~ x, data = sim2)
mod2 <- lm(y ~ x - 1, data = sim2)

sim2

grid <- sim2 %>% 
  data_grid(x) %>% 
  spread_predictions(mod1, mod2)
grid

ggplot(sim2, aes(x)) + 
  geom_point(y = y) + 
  geom_point(
    data = grid, mapping = aes(y = pred, color = model),
    color = "red", size = 4
  ) 

x3 <- model_matrix(y ~ x1 * x2, data = sim3)
x3

all(x3[["x1:x2b"]] == (x3[["x1"]] * x3[["x2b"]]))

x4 <- model_matrix(y ~ x1 * x2, data = sim4)
x4
all(x4[["x1"]] * x4[["x2"]] == x4[["x1:x2"]])

mod1 <- lm(y ~ x1 + x2, data = sim3)
mod2 <- lm(y ~ x1 * x2, data = sim3)

df <- tribble(
  ~x, ~y,
  1, 2.2,
  2, NA,
  3, 3.5,
  4, 8.3,
  NA, 10
)

mod <- lm(y ~ x, data = df)
mod <- lm(y ~ x, data = df, na.action = na.exclude)
nobs(mod)
