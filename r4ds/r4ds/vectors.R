library(tidyverse)

c(-1, 0, 1) / 0

is.na(NaN)

x <- "This is a reasonably long string."
pryr::object_size(x)

y <- rep(x, 1000)
pryr::object_size(y)

?is.finite

!is.infinite(3)

dplyr::near

.Machine$double.eps

.Machine$double.eps
?.Machine

?is_scalar_atomic

sample(10) + 100
runif(10) > 0.5

1:10 + 1:2

tibble(x = 1:4, y = 1:2)
tibble(x = 1:4, y = rep(1:2, 2))

c(x = 1, y = 2, z = 4)
set_names(1:3, c("a", "b", "c"))

x <- c("one", "two", "three", "four", "five")
x[c(1, 1, 5, 5, 5, 2)]
x[c(-3, -1, -5)]
x[c(1, -1)]
x[0]

x <- c(10, 3, NA, 5, 8, 1, NA)
x[!is.na(x)]
x[x %% 2 == 0]
x[]

?is.vector
?is.atomic
?setNames
?set_names

last <- function(x) {
  stopifnot(length(x) >= 1)
  
  return(x[length(x)])
}

not_last <- function(x) {
  if(length(x)) {
    return(x[-length(x)]) 
  } else {
    x
  }
}

only_evens <- function(x) {
  return(x[!is.na(x) & x %% 2 == 0])
}

even_elems <- function(x) {
  return(x[!is.infinite(x) & !is.nan(x) & seq_along(x) %% 2 == 0])
}

not_last(x)
only_evens(x)

x <- c(10, 3, NA, -5, 8, -1, 0, NA)
x[-which(x > 0)]

x <- c(-Inf, -1, 0, 1, Inf, NA, NaN)

which(x)

even_elems(x)

x[x <= 0]
?which


last(1)
last(1:10)

is.atomic(1:10)
x <- 1:10
attr(x, "something") <- TRUE
is.vector(x)
is.atomic(x)

seq_along(x)
not_last(numeric())

even_elems(c(0:4, NA, NaN, Inf))

x <- c(-1:1, Inf, -Inf, NaN, NA)
x[-which(x > 0)]
x[x <= 0]
x <= 0

x > 0
which(x > 0)

x <- list(1, 2, 3)
x

str(x)

x_named <- list(a = 1, b = 2, c = 3)
str(x_named)

y <- list("a", 1L, 1:5, TRUE)
str(y)

z <- list(list(1, 2), list(3, 4))
z

x1 <- list(c(1, 2), c(3, 4))
x2 <- list(list(1, 2), list(3, 4))
x3 <- list(1, list(2, list(3)))

a <- list(a = 1:3, b = "a string", c = pi, d = list(-1, -5))

str(a[1:2])
str(a[4])

str(y[[1]])
str(y[[4]])

diamonds
diamonds[[1]][[1]][[1]]

x <- 1:10
attr(x, "greeting")

attr(x, "greeting") <- "Hi!"
attr(x, "farewell") <- "Bye!"
attributes(x)

attr(x, "greeting") <- list(1, TRUE, "Hi")
attributes(x)

as.Date

methods("as.Date")
getS3method("as.Date", "numeric")

typeof(hms::hms(3600))
attributes(hms::hms(3600))

tibble::tibble(x = list(1, 2, 3, 4, 5), y = list(2, 3, 4, 1, list(5, 6)))
rlang::last_error()

attributes(tibble::tibble(x = list(1, 2, 3, 4, 5), y = list(2, 3, 4, 1, list(5, 6))))
